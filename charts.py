import pandas as pd
import numpy as np
from collections import Counter

"""
Begin constants
"""

# For properly labeling political alignments according with house style
party_dict = {
    'Democratic' : 'Mainstream Democrat',
    'Republican' : 'Mainstream Republican',
    'Strongly Democratic' : "Hardened Democrat",
    'Strongly Republican' : 'Hardened Republican',
    'Unknown' : 'Swing Voter'
}

# Default colors
iqm_colors = ['#FF5D5E', '#37D2AB']
blu_red = ['#0c2950', '#910007']

"""
Begin functions
"""



def erase_gender(df):
    """
    df = DatFrame with multi_index ['Party Affiliation-Likely', Voters_Gender],
         and a count of hoe many people of each party/gender group

    Returns dataframe with party-level data, with gender erased
    """

    tdf = df[:]
    mults = []

    for party in tdf.index.levels[0]:

        edf = tdf.loc[party]
        total = edf['count'].sum()
        print(total, party)
        mults.extend((edf['count'] / total).tolist())

    tdf['MULTS'] = mults

    tdf = tdf.iloc[:, 1:].multiply(np.array(mults), axis=0)

    return tdf.reset_index().groupby('Party Affiliation-Likely').sum().iloc[:, :-2]

def erase_party_and_gender(df):
    """
    df = state-level DataFrame

    returns DataFrame of overall state average
    """

    tdf = df[:]

    total = tdf['count'].sum()
    mults = tdf['count'] / total

    skipnum = Counter(tdf.dtypes)[np.dtype('O')]
    # print(Counter(tdf.dtypes))
    # mults = mults.toarray()
    # mults = mults.reshape(10, 1)
    if 'state' in list(tdf):
        tdf = tdf.drop('state', axis=1)
    print(np.array(mults.tolist()))
    print(tdf.shape)
    print(mults.shape)
    tdf = tdf.iloc[:, skipnum:].multiply(mults, axis=0)

    return tdf.sum().to_frame()



def dem_rep_opp_chart(df, colors = blu_red, state=None, save = False):


    label_dict={
    'climate' : 'Climate Change',
    'min' : 'Minimum Wage Increase',
    'env' : 'Environmental Regulation',
    'fracking' : 'Fracking',
    'gay' : 'Gay Marriage',
    'ACA' : 'Affordable Care Act',
    'death' : 'Death Penalty',
    'renewable' : 'Renewable Energy',
    'legal' : 'Legal Marijuana',
    'path2cit' : 'Path to Citizenship',
    'cops' : 'Police Force Reasonable',
    'aborition' : 'Legal Abortion',
    'liberty' : 'Civil Liberties',
    'ride' : 'Ride Share Regulation',
    'school' : 'School Choice',
    'union' : 'Unions',
    'gun' : 'Gun Control'
}

    h1 = [list(df)[x] for x in range(1, len(list(df)), 2)]
    h2 = [x for x in list(df)[1:] if x not in h1]

    diff_df = pd.DataFrame(df[h1].as_matrix() - df[h2].as_matrix())



    diff_df.columns = h1
    diff_df.index = df['party']

    if not save:
        return diff_df

def dem_rep_opp_data(df, state=None):


    label_dict={
    'climate' : 'Climate Change',
    'min' : 'Minimum Wage Increase',
    'env' : 'Environmental Regulation',
    'fracking' : 'Fracking',
    'gay' : 'Gay Marriage',
    'ACA' : 'Affordable Care Act',
    'death' : 'Death Penalty',
    'renewable' : 'Renewable Energy',
    'legal' : 'Legal Marijuana',
    'path2cit' : 'Path to Citizenship',
    'cops' : 'Police Force Reasonable',
    'aborition' : 'Legal Abortion',
    'liberty' : 'Civil Liberties',
    'ride' : 'Ride Share Regulation',
    'school' : 'School Choice',
    'union' : 'Unions',
    'gun' : 'Gun Control'
}

    h1 = [list(df)[x] for x in range(1, len(list(df)), 2)]
    h2 = [x for x in list(df)[1:] if x not in h1]

    diff_df = pd.DataFrame(df[h1].as_matrix() - df[h2].as_matrix())

    diff_df.columns = h1
    diff_df.index = df['party']
    headers = list((diff_df.max(axis=0) -  diff_df.min(axis=0)).sort_values().index[-5:])
    tdf = diff_df[headers]
    tdf = tdf.round()

    ddict = {}



    ddict['topics'] = [label_dict[x.split('_')[0]] for x in tdf]
    #[str(x) for x in list(diff_df)[:5]]
    ddict['dem'] = tdf.loc['DEM'][:5].tolist()
    ddict['rep'] = tdf.loc['REP'][:5].tolist()
    ddict['min'] = tdf.min().min()
    ddict['max'] = tdf.max().max()

    return ddict
    # # # return tdf[g1]s
