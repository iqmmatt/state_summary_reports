$(document).ready(function() {


Highcharts.chart('container2', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Voters by Gender'
    },
    colors: ['#ff5859', '#33d5ad'],
    xAxis: {
        categories: [
            'Women',
            'Men'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Percent of Registered Voters'
        },
        labels: {
          formatter: function() {
             return this.value+"%";
          }
  }
    },
    exporting: { enabled: false },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0,
            borderWidth: 0
        },
        // series: {
        //     color: '#FF0000'
        // }
    },
    series: [{
        name: '',
        showInLegend: false,
        data: [90, 80],
        colorByPoint: true
        // color: '#FF0000'

    }]
});



Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Party Afiliation'
    },
    xAxis: {
        categories: [
            'Mainstream Democrats',
            'Mainstream Republicans',
            'Hardened Democrats',
            'Hardened Republicans',
            'Swing Voters'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Count (Thousands)'
        }
    },
    exporting: { enabled: false },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Women',
        color: '#ff5859',
        showInLegend: false,
        data: [600, 500, 350, 200, 970]

    }, {
        name:'men',
        color: '#33d5ad',
        showInLegend: false,
        data: [400, 490, 180, 300, 900]

    }]
});









// Thard Graph




Highcharts.chart('container3', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Top issues in VA'
    },

    yAxis: {
        min: 0,
        title: {
            text: '',
            align: 'high'
        },
        labels: {
          formatter: function() {
             return this.value+"%";
          }
      }
    },

    exporting: { enabled: false },
    tooltip: {
        valueSuffix: ' millions'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true,
                align: 'right',
                color: '#FFFFFF',
                x: -10
            }
        },
        column: {
            pointPadding: 0,
            borderWidth: 0
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        enabled: false,
        y: 80,
        floating: true,
        borderWidth: 0,
        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        pointPadding: 0,
        pointPlacement: 0,
        name: 'Women',
        color: '#ff5859',
        data: [70, 75, 40, 90, 60],
        pointPlacement: -0
    },{
        pointPadding: 0,
        pointPlacement: 0,
        name: 'Men',
        color: '#33d5ad',
        data: [73, 90, 60, 30, 55],
        pointPlacement: -0
    }]

});






});
