from flask import Flask, render_template, request, g
from charts import *
from auto_text import *
# from big_analysis.toolkit import make_party_county_map
import pandas as pd
import os
from state_dict import states

app = Flask(__name__)

"""
This file serves content to the javascript front-end put together by the team in India.

The file was originally built with a different purpose (to generate static pages). As
a result, it has some artefacts in it that should be ignored.
"""

@app.route('/<state>')
def generate_page(state):
    """
    state = str, two-letter state abbreviation

    This is the master page-generation funciton. It extracts data from csvs
    and plugs the data into other functions to generate charts and paragraphs.

    It returns data to an html file via Flask.
    """

    print('THIS IS THE STATE',  state)

    with open('./text.json', 'r') as f:
        js = json.load(f)

    js = js[state]
    # Getting files for the targeted state
    # state_files = [x for x in os.listdir('./static/state_summaries') if state in x]
    # print(os.listdir('./static/state_summaries'))
    # state_files = [x for x in state_files if x[:2] == state or x[-2:] == state]
    #
    # state_sum = [x for x in state_files if "state_summary" in x][0]


    # map = make_party_county_map(state)
    map = 'static/maps/' + state.upper() + '_political_county_map.png'

    # Preparing the state summary dataframe for consumption
    df = pd.read_csv('./static/state_summaries/' + state.upper() + '_state_summary.csv')
    df = df.dropna()
    df.set_index(['Voters_Gender', 'Party Affiliation-Likely'], inplace=True)

    # Generating chart and paragraph about gender breakdown
    # ch1 =  men_v_women_registration(df, state=state, save = True)
    tdf = df.groupby('Voters_Gender').sum()
    mvwr = [tdf.loc[x]['count']/tdf['count'].sum()*100 for x in ['F', 'M']]
    print(mvwr)
    para1 = gender_breakdown_para(df, state=state)

    # Generating chart and paragraph about party affiliations
    # ch2 = men_v_women(df, 'count', pct_labels = False,
    #                 title = 'Party Afiliation', save = True, state=state)
    mvwps = [df.loc[x]['count'].tolist() for x in ['F', 'M']]
    print(mvwps)
    para2 = party_breakdown_para(df, state)

    # Generating chart and paragraph about top issues in the state
    ndf = pd.read_csv('./spark_averages/'+state+'_averages.csv')
    ddict = dem_rep_opp_data(ndf, state)
    print(ddict)
    ndf = dem_rep_opp_chart(ndf, state=state, save = False)

    para3 = divisive_issues_para(ndf, state)

    # Preparing errant issue csv for consumption
    odf = pd.read_csv('./static/errant_issues_by_state_party_gender.csv')
    odf = odf[odf['state'] == state]
    odf = pd.concat([odf.iloc[:, :4], odf.iloc[:, -3:] * 100 ], axis=1)
    odf = odf.round(1)

    # Getting errant issues for men and women in the state
    outliers_m = []
    outliers_w = []
    pos_dict = {
    'Unknown' : 'No Clear Opinion'
    }
    for i, x in odf[odf['Voters_Gender'] == 'M'].iterrows():
        eiss = x['Errant_issue']
        ei2 = eiss.split('_')[0]
        short = hdict.get(ei2, ei2) + ': ' + pos_dict.get(eiss.split('_')[1],eiss.split('_')[1])

        vals = [
        party_dict[x['Party Affiliation-Likely']],
        x['Voters_Gender'],
        short.title(),
        x['St_avg'],
        x['US_avg']
        ]
        outliers_m.append(vals)

    for i, x in odf[odf['Voters_Gender'] == 'F'].iterrows():
        eiss = x['Errant_issue']
        ei2 = eiss.split('_')[0]
        short = hdict.get(ei2, ei2) + ': ' + pos_dict.get(eiss.split('_')[1],eiss.split('_')[1])

        vals = [
        party_dict[x['Party Affiliation-Likely']],
        x['Voters_Gender'],
        short.title(),
        # x['Errant_issue'],
        x['St_avg'],
        x['US_avg']
        ]
        outliers_w.append(vals)

    # Generating Introductory Paragraph
    intro = intro_para(states[state])

    print(outliers_w)
    # Generating html document
    # with app.app_context():
    return render_template('index.html',
            map = map,
            mvwps = mvwps,
            mvwr = mvwr,
            intro = js['intro'],
            ddict = ddict,
            para1 = js['gender'],
            para2 = js['party'],
            para3 = js['divisive'],
            outliers_m = outliers_m,
            outliers_w = outliers_w,
            all_states = list(states.keys()),
            state = state,
            fulls = states[state])


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)
