## This module runs on python 3.6.

# State-by-state reports

This module takes a collection of data and generates a report for each state,
including each of the following:

1. A county-map of party lean (pre-generated)
2. A bar chart of voter registration by gender (JavaScript, from data)
3. A bar chart of political lean breakdown (JavaScript, from data)
4. A horizontal bar chart on the most divisive issues in the state (JavaScript, from data)
5. A table of outlier issues across 10 demographic groups (table, from data)
6. Several paragraphs explaining the charts (from text.json)
...(text.json was algorithmically pre-generated, but may later be human-updated)
