import pandas as pd
import numpy as np
from state_dict import states, label_dict
import json

blu_red = ['#0c2950', '#910007']

"""
This file works in conjunciton with page_generator.py to return text for the state pages
"""

"""
Begin constatns section
"""

gdict = {
    'F' : 'women',
    'M' : 'men'}

nick_dict = {}
cap_dict = {}
nickscaps = open('./state_nicks_caps.csv')
for k in nickscaps.read().split('\n')[1:]:
    r = k.split(',')
    nick_dict[r[0]] = r[2]
    cap_dict[r[0]] = r[1]

hdict = {
    'Trump-Attention By Media On Russian Connections' : "media coverage of Trump-Russian Connections",
    'Climate Change Believer' : 'climate change',
    'Govt National Healthcare Plan Covering Everyone' : 'government healthcare covering everyone',
    'Trump-Concerned About His Potential Business Conflicts' : "Trump's potential business conflicts",
    'Allow Transgender Bathroom' : 'transgender bathroom usage',
    "Affordable Care Act" : 'the Affordable Care Act',
    'Drug Price Limits Through Govt Regulation' : 'government regulation of drug prices',
    'Opioid Crisis Concern' : 'the opiod crisis',
    'Common Core National Educational Curriculum' : 'Common Core',
    'Environment' : 'the environment',
    'Minimum Wage Increase' : 'raising the minimum wage',
    'Income Inequality Major Problem' : 'whether income inequality is a major problem',
    'Undocumented Immigrants-Pathway to Citizenship' : 'a pathway to citizenship for undocumented immigrants',
    'Border Wall With Mexico' : 'the border wall with Mexico',
    'Muslim Ban' : "President Trump's Muslim travel ban",
    'Rideshare Govt Regulation' : 'government regulation of the rideshare industry',
    'Citizens United Supreme Court Ruling' : "the Citizens United Supreme Court ruling",
    'Civil Liberties Versus Protection From Terrorists' : 'civil liberties in a post 9/11 world',
    'Social Security Reduce or Reform to Preserve' : 'Social Security',
    'Tax Cuts-Federal' : 'federal tax cuts',
    'Tobacco Tax' : 'tobacco taxes',
    'U.S. Military Intervention Overseas' : 'U.S. military intervention overseas',
    'Political Redistricting Method' : 'political redistricting',
    'Govt Bailouts of Large Businesses' : 'government bailouts of large businesses',
    'Govt Management of Land and Wildlife' : 'government management of land and wildlife',
}

"""
Begin functions section
"""

def house_style(phrase):
    """
    phrase = str
    state = str, two leter abbreviation for the state

    Checks the phrase agains the house style dictionary. If the phrase is in the
    house style dictionary, returns the house style. Otherwise, it returns a
    lower-case version of the phrase.
    """
    if phrase in hdict.keys():
        return hdict[phrase]
    else:
        return phrase.lower()

def gender_breakdown_para(df, state):
    """
    df = DataFrame of state-level data

    Returns str, paragraph summarizing gender breakdowns in the state
    """

    # Getting percentage of men and women in state
    tdf = df[:].reset_index().groupby('Voters_Gender').sum()
    tdf = tdf / tdf['count'].sum()

    # Getting gender with highest and lowest pct, and the gap between them
    top = gdict[tdf.idxmax()[0]]
    bot = gdict[tdf.idxmax()[1]]
    gap = int(round( (tdf['count'].max() - tdf['count'].min()) * 100, 0))

    # Building the first string
    s1 = '%s outnumber %s statewide, with a registration advantage of %s percent.' % (top, bot, gap)
    s1 = s1[0].upper() + s1[1:]

    # Getting the gender breakdown of 'swing' voters
    udf = df[:].reset_index().set_index(['Party Affiliation-Likely', 'Voters_Gender'])
    udf = udf.loc['Unknown'][['count']]
    udf = udf / udf['count'].sum()

    # Getting gender with highest and lowest pct, and the gap between them
    utop = gdict[udf.idxmax()[0]]
    ubot = gdict[udf.idxmin()[0]]
    ugap = int(round( (udf['count'].max() - udf['count'].min()) * 100, 0))

    # Building the second phrase
    s2 = 'Among swing voters, %s outnumber %s by %s percent.' % (utop, ubot, ugap)


    return ' '.join([s1, s2])


def party_breakdown_para(df, state):
    """
    df = DataFrame of state-level data
    state = str, two leter abbreviation for the state

    Returns str, paragraph summarizing gender breakdowns in the state
    """

    # Getting counts of people in each party group
    tdf = df[:].reset_index().groupby('Party Affiliation-Likely').sum()

    # Combining the Republican and Democratic groups for simplification
    dem = tdf.loc[['Democratic', 'Strongly Democratic']]['count'].sum()
    rep = tdf.loc[['Republican', 'Strongly Republican']]['count'].sum()
    unk = tdf.loc['Unknown']['count'].sum()
    tot = tdf['count'].sum()

    # Getting party with most and least members within the state
    plist = ['Democrat', 'Republican']
    d_r = np.array([dem, rep])
    top = plist[d_r.argmax()]
    bot = plist[d_r.argmin()]

    # Getting percentages for each party
    topp = d_r.max() / tot
    botp = d_r.min() / tot

    # Getting top party advantage, rounding to one decimal place
    gap = str(round( (topp-botp) * 100, 1))

    # Choosing sentence structure based on the gap bewtween the parties
    if float(gap) < 2:
        s1 = 'Democrats and Republicans are virtually tied in %s, where %ss outnumber %ss by just %s percent.' % (states[state], top, bot, gap)
    else:
        s1 = '%ss outnumber %ss by %s percent in %s.' % (top, bot, gap, states[state])
    s1 = s1[0].upper() + s1[1:]

    # Generating sentence about the percentage of swing voters in the state
    upct = str(round(unk/tot * 100, 0))[:-2]
    s2 = 'Swing voters make up about %s percent of registered voters in the state.' % (upct)

    return ' '.join([s1, s2])


def top_issues_para(df, state, issues):
    """
    df = DataFrame of state-level data
    state = str, two letter abbreviation for a state
    issues = list of strings, table headers for HaystaqDNA. Each should include
            '_Unknown'

    Returns str, paragraph summarzing how people feel about the top issue in the
    state and listing several other top issues.
    """

    # Getting the stem for the strongest issue (text before '_')
    top = df[issues].idxmin(axis=1)[0].split('_')[0]

    # Getting percentages for how people feel on top issue
    topdf = df[[x for x in list(df) if top in x and '_Unk' not in x]]
    topdf = (topdf * 100).round(1)

    # Getting opposing views on the target issue
    i1 = list(topdf)[0].split('_')[1].lower().strip()
    i2 = list(topdf)[1].split('_')[1].lower().strip()

    # Getting pct of state residents with each view
    n1 = str(topdf[list(topdf)[0]][0])
    n2 = str(topdf[list(topdf)[1]][0])

    #Building strings about the top issue in the state.
    s1 = 'According to our data, the issue that voters in %s feel most strongly about is %s.' % (state, house_style(top))
    s2 = 'Fully %s%% of %s voters say "%s," %s%% say" %s."' % (n1, states[state], house_style(i1), n2, house_style(i2))

    # Building sentence about other top issues
    s3 = 'Other top issues include ' + ', '.join([house_style(x.split('_')[0]) for x in issues[1:-1]]) + \
    ' and ' + house_style(issues[-1].split('_')[0]) + '.'

    return ' '.join([s1, s2, s3])

def intro_para(state):
    """
    state = str, full state name

    returns short paragraph introducing the state
    """

    cap = cap_dict[state]
    nick = nick_dict[state][4:]

    s1 = '%s voters will have a lot to say this November. Their votes could be a signal for real change in %s as well as Washington D.C.' % (state, cap)
    s2 = 'To get a sense of %s voters, IQM crunched our numbers and the data from our partners to generate this topline report.' % (nick)

    return ' '.join([s1, s2])

def divisive_issues_para(df, state):
    """
    df = dataframe of divisive issues in the state

    state = two-letter abbreviation
    """

    diff_df = (abs(df.loc['DEM'] - df.loc['REP']))
    mlbl = diff_df.idxmax()
    md = label_dict[mlbl.split('_')[0]].lower()

    nlbl = diff_df.idxmin()
    nd = label_dict[nlbl.split('_')[0]].lower()

    pterm = 'support'
    nterm = 'oppose'

    if md == 'climate change':
        pterm  = 'believe in'
        nterm = 'doubt'
    print(df.index)
    pos1 = [pterm if df.loc[p][mlbl] > 0 else nterm for p in ['REP', 'DEM']]

    s1 = 'According to our data, the most divisive issue in %s is %s.' % (states[state], md)
    s2 = 'Republicans %s %s, while Democrats %s it.' % (pos1[0], md, pos1[1])

    mnpct = str( int (round(diff_df[nlbl], 0)))
    mxpct = str( int( round(diff_df[mlbl], 0)))

    s3 = 'Democrats and Republicans in %s came closest to reaching consensus on %s.' % (states[state], nd)
    s4 = 'Only %s percentage points separated their average opinions, compared to %s%% on %s' % (mnpct, mxpct, md)


    return ' '.join([s1, s2, s3, s4])

def make_state_json():

    state_text = {}

    for st in states.keys():

        try:
            sdict = {}

            sdict['intro'] = intro_para(states[st])

            df = pd.read_csv('./state_reports/static/state_summaries/' + st.upper() + '_state_summary.csv')
            df = df.dropna()
            # print(list(df))
            df.set_index(['Voters_Gender', 'Party Affiliation-Likely'], inplace=True)
            tdf = df.groupby('Voters_Gender').sum()

            sdict['gender'] = gender_breakdown_para(df, st)
            sdict['party'] = party_breakdown_para(df, st)

            ndf = pd.read_csv('./state_reports/spark_averages/'+st+'_averages.csv')
            ddict = dem_rep_opp_data(ndf, st)
            # print(ddict)
            ndf = dem_rep_opp_chart(ndf, state=st, save = False)

            sdict['divisive'] = divisive_issues_para(ndf, st)
            # print(sdict)
            state_text[st] = sdict
        except:
            print(st)
            pass

    print(state_text)
    j = json.dumps(state_text, indent=4)
    with open('result.json', 'w') as fp:
        fp.write(j)

def dem_rep_opp_data(df, state=None):


    label_dict={
    'climate' : 'Climate Change',
    'min' : 'Minimum Wage Increase',
    'env' : 'Environmental Regulation',
    'fracking' : 'Fracking',
    'gay' : 'Gay Marriage',
    'ACA' : 'Affordable Care Act',
    'death' : 'Death Penalty',
    'renewable' : 'Renewable Energy',
    'legal' : 'Legal Marijuana',
    'path2cit' : 'Path to Citizenship',
    'cops' : 'Police Force Reasonable',
    'aborition' : 'Legal Abortion',
    'liberty' : 'Civil Liberties',
    'ride' : 'Ride Share Regulation',
    'school' : 'School Choice',
    'union' : 'Unions',
    'gun' : 'Gun Control'
}

    h1 = [list(df)[x] for x in range(1, len(list(df)), 2)]
    h2 = [x for x in list(df)[1:] if x not in h1]

    diff_df = pd.DataFrame(df[h1].as_matrix() - df[h2].as_matrix())

    diff_df.columns = h1
    diff_df.index = df['party']
    headers = list((diff_df.max(axis=0) -  diff_df.min(axis=0)).sort_values().index[-5:])
    tdf = diff_df[headers]
    tdf = tdf.round()

    ddict = {}



    ddict['topics'] = [label_dict[x.split('_')[0]] for x in tdf]
    #[str(x) for x in list(diff_df)[:5]]
    ddict['dem'] = tdf.loc['DEM'][:5].tolist()
    ddict['rep'] = tdf.loc['REP'][:5].tolist()
    ddict['min'] = tdf.min().min()
    ddict['max'] = tdf.max().max()

    return ddict

def dem_rep_opp_chart(df, colors = blu_red, state=None, save = False):


    label_dict={
    'climate' : 'Climate Change',
    'min' : 'Minimum Wage Increase',
    'env' : 'Environmental Regulation',
    'fracking' : 'Fracking',
    'gay' : 'Gay Marriage',
    'ACA' : 'Affordable Care Act',
    'death' : 'Death Penalty',
    'renewable' : 'Renewable Energy',
    'legal' : 'Legal Marijuana',
    'path2cit' : 'Path to Citizenship',
    'cops' : 'Police Force Reasonable',
    'aborition' : 'Legal Abortion',
    'liberty' : 'Civil Liberties',
    'ride' : 'Ride Share Regulation',
    'school' : 'School Choice',
    'union' : 'Unions',
    'gun' : 'Gun Control'
}

    h1 = [list(df)[x] for x in range(1, len(list(df)), 2)]
    h2 = [x for x in list(df)[1:] if x not in h1]

    diff_df = pd.DataFrame(df[h1].as_matrix() - df[h2].as_matrix())



    diff_df.columns = h1
    diff_df.index = df['party']

    if not save:
        return diff_df
    headers = list((diff_df.max(axis=0) -  diff_df.min(axis=0)).sort_values().index[-5:])
    tdf = diff_df[headers]

    x1 = [x*1.5 for x in range(len(headers))]
    # print(x1)
    x2 = [x*1.5+ 0.4 for x in range(len(headers))]
    xl = [x+ 0.2 for x in range(len(headers))]
    # # # return tdf[g1]s
    f = plt.figure(figsize=(10,5))
    ax = f.add_subplot(1,1,1)
    ax.barh(x1, tdf.loc['DEM'], 1, label='Democrats', color=colors[0])
    ax.barh(x1, tdf.loc['REP'].values, 1, label='Republicans', color = colors[1] )


    # lim = diff_df.max().max()
    # if abs(diff_df.min().min()) > lim:
    #     lim = abs(diff_df.min().min())
    # # plt.yticks(xl, [x.split('_')[0] for x in headers], rotation=0, fontsize=12)
    # plt.xlim(-lim, lim)

    for x, t in zip(x1, headers):
    #     plt.text(.01, x, t.split('_')[0], rotation=0, va = 'center', ha='left', fontsize = 10)
    #     plt.text(.01, x + 0.4, z.split('_')[1], rotation=0, va = 'center', ha='left', fontsize = 10)
        if abs(tdf.max().max()) > abs(tdf.min().min()):
            plt.text(0.5, x + 0, label_dict[t.split('_')[0]], rotation=0, va = 'center', ha='left', fontsize = 12, color='w')
        else:
            plt.text(-0.5, x + 0, label_dict[t.split('_')[0]], rotation=0, va = 'center', ha='right', fontsize = 12, color='w')

    plt.text(tdf.max().max() + 0.5, 3, "Support / Believe", rotation = -90, verticalalignment = 'center', fontsize=20)
    plt.text(tdf.min().min() - 0.5, 3, "Oppose / Doubt", rotation = 90, verticalalignment = 'center', fontsize=20, horizontalalignment='right')

    ax.get_yaxis().set_visible(False)
    vals = ax.get_xticks()
    ax.legend()
    ax.set_xticklabels(['{:,.0%}'.format(x/100) for x in vals])
    plt.title('Most Divisive Issues in ' + state)
    plt.xlabel('Mean Support Minus mean Opposition')

    if save:
        nm = state + '_divisive_issues.png'
        plt.savefig(nm, bbox_inches='tight')

        return nm, diff_df
